-- ------------ Write CREATE-DATABASE-stage scripts -----------

CREATE SCHEMA IF NOT EXISTS activity;



CREATE SCHEMA IF NOT EXISTS dbo;



CREATE SCHEMA IF NOT EXISTS license;



CREATE SCHEMA IF NOT EXISTS service;



-- ------------ Write CREATE-TYPE-stage scripts -----------

CREATE TYPE dbo.stringcollection$aws$t AS (
value TEXT
);


CREATE TYPE license.stringcollection AS (
    value TEXT
);
-- ------------ Write CREATE-DOMAIN-stage scripts -----------

CREATE DOMAIN dbo.stringcollection AS dbo.stringcollection$aws$t [] NOT NULL;



-- ------------ Write CREATE-TABLE-stage scripts -----------

CREATE TABLE activity.log(
    id BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY,
    inputparams VARCHAR(200) NOT NULL,
    outputparams VARCHAR(200) NOT NULL,
    createddatetimeutc TIMESTAMP(4) WITHOUT TIME ZONE NOT NULL DEFAULT timezone('UTC', LOCALTIMESTAMP(6))
)
        WITH (
        OIDS=FALSE
        );
