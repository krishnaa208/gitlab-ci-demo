import boto3
from botocore.exceptions import ClientError
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
import os
import base64
import json
from urllib.parse import urlencode


if 'Environment' not in os.environ:
    os.environ['Environment'] = 'dev'

os.environ['CURRENT_ENVIRONMENT_NAME'] = os.environ['Environment'].lower()
# configurable owner emails
os.environ['ENVIORNMENT_OWNER'] = '''
{
    "dev" : "dev@yopmail.com,dev2@yopmail.com",
    "dev_stable"  : "dev_stable@yopmail.com",
    "qa"  : "qa@yopmail.com",
    "demo": "demo@yopmail.com"
}
'''
os.environ['SES_EMAIL_BODY'] = '''
<!DOCTYPE html>
<html>
<head>
</head>
<body>
    <p><b>Hello {CURRENT_ENVIRONMENT_OWNER},</b></p>
    <br/>
    <p> New Tag has been build successfully on {CURRENT_ENVIRONMENT_NAME}
        <p>
            <br/>
            <p>
                If you want to promote to {NEXT_ENVIRONMENT_NAME} enviromnet then click on Ask To Promote button below
            </p>
            <br/>
            <table cellspacing="0">
                <tr>
                    <td style="border: 1px solid #dddddd; text-align: left; padding: 10px 40px;">Project </td>
                    <td style="border: 1px solid #dddddd; text-align: left; padding: 10px 40px;">{CI_PROJECT_NAME}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #dddddd; text-align: left; padding: 10px 40px;">Ref Branch/Tag</td>
                    <td style="border: 1px solid #dddddd; text-align: left; padding: 10px 40px;">{CI_COMMIT_REF_NAME}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #dddddd; text-align: left; padding: 10px 40px;">Author </td>
                    <td style="border: 1px solid #dddddd; text-align: left; padding: 10px 40px;">{GITLAB_AUTHOR_EMAIL}</td>
                </tr>
                <tr>
                    <td colspan="2" style="border: 1px solid #dddddd; text-align: center; padding: 10px 40px;" >
                         <a style="text-decoration: none;color:#FFF;border: 1px solid #4CAF50; background: #4CAF50; padding:7px;" href="{TRIGGER_ENDPOINT_LAMBDA}?{TRIGGER_PARAMS}">Ask To Promote</a>
                    </td>
                </tr>
            </table>
            <br/> Thanks And Regards.
</body>
</html>
'''
os.environ['SES_EMAIL_FROM'] = 'gitlab@devcpisimplifi.com'
os.environ['SES_EMAIL_SUBJECT'] = 'Sending email with Attachment'

os.environ['TRIGGERED_BY'] = 'TRIGGERED_BY_name'

# constants
os.environ['TRIGGER_ENDPOINT_LAMBDA'] = 'http://127.0.0.1:8000'
os.environ['TRIGGER_TOKEN'] = 'baaccfe9adbb5ca45925e8d065f596'
os.environ['SES_REGION'] = 'us-east-1'

# # GITLAB PREDEFINED VARIABLES
# os.environ['CI_COMMIT_REF_NAME'] = 'master'
# os.environ['CI_PROJECT_NAME'] = 'gitlab-ci-demo'
# os.environ['GITLAB_USER_EMAIL'] = 'authoer@yopmail.com'
# os.environ['GITLAB_USER_LOGIN'] = 'krishnaa208'
# os.environ['CI_PROJECT_URL'] = 'gitlab.com'
# os.environ['CI_PROJECT_ID'] = '18085520'




class Template:

    def __init__(self):
        self.message = MIMEMultipart()
        self.attachment_list = []

    def get_attachments_list(self):
        return self.attachment_list

    def attach_body(self, content, content_type='html'):
        part = MIMEText(content, content_type)
        self.message.attach(part)

    def attach_file(self, file_path, attachment_name=None):
        try:
            if not os.path.exists(file_path):
                raise IOError()
            if attachment_name is None:
                attachment_name = os.path.split(file_path)[-1]
            txt = open(file_path, 'r').read()
            part = MIMEApplication(txt)
            part.add_header('Content-Disposition', 'attachment',
                            filename=attachment_name)
            self.message.attach(part)
            self.attachment_list.append(attachment_name)
        except IOError as ie:
            print('IOError ',str(ie))
        except Exception as e:
            print('Error while attaching file to template',str(e))

    def get_content(self):
        return self.message


class SES:
    def __init__(self, region='us-east-1'):
        self.client = boto3.client(service_name='ses', region_name=region)

    def send_email(self, template, _from, _to):
        message = template.get_content()
        try:
            result = self.client.send_raw_email(
                Source=_from, Destinations=[_to], RawMessage={'Data': message.as_string(), })
            if 'ErrorResponse' in result:
                raise Exception('Error While Sending Email')
            return { 'message': 'Email Sent Successfully' }
        except ClientError as e:
            return {'message': e.response['Error']['Message'] }


def next_environment(current_build):
    items = json.loads(os.environ['ENVIORNMENT_OWNER'])
    li = list(items.keys())
    li_len = len(li)
    index = li.index(current_build)
    if index+1 < li_len:
        return li[index+1]
    exit(0)

def get_email_list():
    try:
        return json.loads(os.environ['ENVIORNMENT_OWNER'])[os.environ['CURRENT_ENVIRONMENT_NAME']].split(',')
    except Exception as e:
        print(str(e))

def get_email_body(to_email, lambda_trigger_params):
   return os.environ['SES_EMAIL_BODY'].format_map({
        'CURRENT_ENVIRONMENT_OWNER':to_email,
        'CURRENT_ENVIRONMENT_NAME':os.environ['CURRENT_ENVIRONMENT_NAME'],
        'NEXT_ENVIRONMENT_NAME': next_environment(os.environ['CURRENT_ENVIRONMENT_NAME']),
        'CI_PROJECT_NAME':os.environ['CI_PROJECT_NAME'],
        'CI_COMMIT_REF_NAME':os.environ['CI_COMMIT_REF_NAME'],
        'GITLAB_AUTHOR_EMAIL':os.environ['GITLAB_USER_EMAIL'],
        'TRIGGER_ENDPOINT_LAMBDA':os.environ['TRIGGER_ENDPOINT_LAMBDA'],
        'TRIGGER_PARAMS': lambda_trigger_params
    })

def get_template(to_email):
    template = Template()
    lambda_type = 'promote_mail'
    try:
        tos = json.loads(os.environ['ENVIORNMENT_OWNER'])[
            next_environment(os.environ['CURRENT_ENVIRONMENT_NAME'])]
    except:
        tos = None

    deploy_lambda_trigger_params = urlencode({
        'pid': os.environ['CI_PROJECT_ID'],
        'project_name': os.environ['CI_PROJECT_NAME'],
        'ref': os.environ['CI_COMMIT_REF_NAME'],
        'token': os.environ['TRIGGER_TOKEN'][::-1],
        'current_env':  next_environment(os.environ['CURRENT_ENVIRONMENT_NAME']),
        'previous_env': os.environ['CURRENT_ENVIRONMENT_NAME'],
        'to_email': to_email,
        'froms': os.environ['SES_EMAIL_FROM'],
        'tos': tos,
        'lambda_type': lambda_type,
        'author': os.environ['GITLAB_USER_EMAIL']
    })

    email_body = get_email_body(to_email, deploy_lambda_trigger_params)
    template.attach_body(email_body)
    return template

        
def send_batch_email(ses_client, _from, to_list):
    print('+ ===========================================================+')
    print('| Current Enviornment Build : ', os.environ['CURRENT_ENVIRONMENT_NAME'])
    print('| Current Ref Tag/Branch    : ', os.environ['CI_COMMIT_REF_NAME'])
    print('| Email From Address        : ', os.environ['SES_EMAIL_FROM'])
    if 'TRIGGRED_BY' in os.environ:
        print('| Pipeline Triggred By      : ', os.environ['TRIGGRED_BY'])     

    for index, to_email in enumerate(to_list):
        template = get_template(to_email)
        res = ses_client.send_email(template, _from, to_email)
        print('| -----------------------------------------------------------|')
        print('|', +index+1, '   send to              : ', to_email)
        print('|      send results         : ', res['message'])
    print('+ ===========================================================+')

def main():
    ses_client = SES()
    from_email = os.environ['SES_EMAIL_FROM']
    to_emails = get_email_list()
    send_batch_email(ses_client,from_email,to_emails)
main()

